import {SHORTEN_URL_SUCCESS} from "./actions";

const initialState = {
    shortUrl: ''
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case SHORTEN_URL_SUCCESS:
            return {...state, shortUrl: action.data.shortUrl};
        default:
            return state;
    }
};

export default reducer;