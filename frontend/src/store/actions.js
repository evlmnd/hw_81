import axios from '../axios-shortUrl';

export const SHORTEN_URL_SUCCESS = 'SHORTEN_URL_SUCCESS';

export const shortenUrlSuccess = data => ({type: SHORTEN_URL_SUCCESS, data});

export const shortenUrl = url => {
    return dispatch => {
        axios.post('/', url).then(response => {
            dispatch(shortenUrlSuccess(response.data));
        })
    }
};

export const openUrl = url => {
    return () => {
        axios.get('/' + url).catch(e => console.log(e));
    }
};