import React, {Component, Fragment} from 'react';
import ShortUrl from "./components/ShortUrl";

class App extends Component {
  render() {
    return (
      <Fragment>
        <ShortUrl/>
      </Fragment>
    );
  }
}

export default App;
