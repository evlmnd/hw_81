import React, {Component} from 'react';
import {connect} from "react-redux";
import {openUrl, shortenUrl} from "../store/actions";
import Response from "./Response/Response";

class ShortUrl extends Component {
    state = {
        originalUrl: ''
    };

    inputChangeHandler = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        })
    };

    submitHandler = (event) => {
        event.preventDefault();
        this.props.onShortenUrl({originalUrl: this.state.originalUrl});
    };

    onClickUrl = () => {
        this.props.onOpenUrl(this.props.shortUrl)
    };

    render() {
        return (
            <div>
                <h1>Shorten your link!</h1>
                <form onSubmit={this.submitHandler}>
                    <input type="text" placeholder="Enter URL here"
                           onChange={this.inputChangeHandler}
                           name="originalUrl"
                           value={this.state.originalUrl}
                    />
                    <button type="submit">Shorten!</button>
                </form>
                {
                    this.props.shortUrl.length > 0 ?
                        <Response
                            shortUrl={this.props.shortUrl}
                            clickUrl={this.onClickUrl}
                        /> : null
                }
            </div>
        );
    }
}

const mapStateToProps = state => ({
    shortUrl: state.shortUrl
});

const mapDispatchToProps = dispatch => ({
    onShortenUrl: url => dispatch(shortenUrl(url)),
    onOpenUrl: url => dispatch(openUrl(url))
});

export default connect(mapStateToProps, mapDispatchToProps)(ShortUrl);