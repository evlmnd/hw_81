import React, {Component} from 'react';

class Response extends Component {
    render() {
        return (
            <div>
                <h3>Your link now looks like this:</h3>
                <a href={`http://localhost:8000/${this.props.shortUrl}`}>{this.props.shortUrl}</a>
            </div>
        );
    }
}

export default Response;