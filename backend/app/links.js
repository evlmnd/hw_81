const express = require('express');
const nanoid = require('nanoid');
const Link = require('../models/Link');

const links = express.Router();

links.post('/', (req, res) => {
    const link = new Link(req.body);
    link.shortUrl = nanoid(6);

    link.save().then(result => {
        res.send(result);
    })
});

links.get('/:shortUrl', (req, res) => {
    Link.findOne({shortUrl: req.params.shortUrl}).then(result => {
        if (result) {
            res.status(301).redirect(result.originalUrl);
        } else {
            res.sendStatus(404);
        }
    }).catch(() => {
        res.sendStatus(500)
    });
});

module.exports = links;


